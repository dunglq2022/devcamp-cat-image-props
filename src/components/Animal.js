import { Component } from "react";
import cat from '../asset/images/cat.jpg'

class Animal extends Component{
    render() {
        let {kind} = this.props
        return(
            <div>
                <h1>Animal</h1>
                {kind === 'cat' ? <img src={cat} alt="cat"></img> : <p className="text-primary" style={{fontSize: 30}}>meow not found :)
</p> }
            </div>
        )
    }
}

export default Animal;